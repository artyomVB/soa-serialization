import timeit

setup_yaml_ser_code = """import yaml
d = {
    'words': '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    ''',
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}
"""
yaml_ser_code = "yaml.dump(d, Dumper=yaml.Dumper)"

setup_yaml_deser_code = """import yaml
d = {
    'words': '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    ''',
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}
with open('file_yaml', 'w+') as f:
    f.write(yaml.dump(d, Dumper=yaml.Dumper))
print('Serialized data len:', len(yaml.dump(d, Dumper=yaml.Dumper)))
"""
yaml_deser_code = """with open('file_yaml', 'r') as f:
    yaml.load(f.read(), Loader=yaml.Loader)
"""

print('Serialization time:', timeit.timeit(yaml_ser_code, setup=setup_yaml_ser_code, number=100) / 100)
print('Deserialization time:', timeit.timeit(yaml_deser_code, setup=setup_yaml_deser_code, number=100) / 100)