import timeit

setup_msgpack_ser_code = """import msgpack
d = {
    'words': '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    ''',
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}
"""
msgpack_ser_code = "msgpack.packb(d)"

setup_msgpack_deser_code = """import msgpack
d = {
    'words': '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    ''',
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}
with open('file_mp', 'wb+') as f:
    f.write(msgpack.packb(d))
print('Serialized data len:', len(msgpack.packb(d)))
"""
msgpack_deser_code = """with open('file_mp', 'rb') as f:
    msgpack.unpackb(f.read())
"""

print('Serialization time:', timeit.timeit(msgpack_ser_code, setup=setup_msgpack_ser_code, number=1000) / 1000)
print('Deserialization time:', timeit.timeit(msgpack_deser_code, setup=setup_msgpack_deser_code, number=1000) / 1000)
