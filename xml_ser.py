import timeit

struct_for_serialization = '''d = {
    'words': """
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    """,
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}'''

setup_xml_ser_code = "import xml_help.xml_func as xml_func\n%s\n" % struct_for_serialization
xml_ser_code = "xml_func.ET.tostring(xml_func.from_dict_to_etree(d))\n"
setup_xml_deser_code = \
    "import xml_help.xml_func as xml_func\n" \
    "%s\nwith open('file_xml', 'wb+') as f:\n    f.write(xml_func.ET.tostring(xml_func.from_dict_to_etree(d)))\n" \
    "print('Serialized data len:',len(xml_func.ET.tostring(xml_func.from_dict_to_etree(d))))" % struct_for_serialization
xml_deser_code = "with open('file_xml', 'rb') as f:\n    s = f.read()\nxml_func.ET.fromstring(s)"
print('Serialization time:', timeit.timeit(xml_ser_code, setup=setup_xml_ser_code, number=1000) / 1000)
print('Deserialization time:', timeit.timeit(xml_deser_code, setup=setup_xml_deser_code, number=1000) / 1000)



