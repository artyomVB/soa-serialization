import xml.etree.ElementTree as ET


def from_dict_to_etree(d):
    root = ET.Element("data")
    for key in d:
        tmp = ET.SubElement(root, key)
        tmp.text = str(d[key])
    return root


def from_etree_to_dict(etree):
    d = {}
    for elem in etree.iter():
        if elem.text:
            d[elem.tag] = elem.text
    return d

