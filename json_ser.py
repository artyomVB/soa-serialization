import timeit

struct_for_serialization = '''d = {
    'words': """
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    """,
    'list': list(range(100)),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}'''

setup_json_ser_code = "import json\n%s\n" % struct_for_serialization
json_ser_code = "json.dumps(d)\n"
setup_json_deser_code = \
    "import json\n%s\nwith open('file_json', 'w') as f:\n    f.write(json.dumps(d))\nprint('Serialized data len:', " \
    "len(json.dumps(d)))" % struct_for_serialization
json_deser_code = "with open('file_json', 'r') as f:\n    s = f.read()\njson.loads(s)"
print('Serialization time:', timeit.timeit(json_ser_code, setup=setup_json_ser_code, number=1000) / 1000)
print('Deserialization time:', timeit.timeit(json_deser_code, setup=setup_json_deser_code, number=1000) / 1000)
