print("Native serialization")
import native_ser
print()
print("JSON serialization")
import json_ser
print()
print("XML serialization")
import xml_ser
print()
print("Protobuf serialization")
import protubuf_ser
print()
print("YAML serialization")
import yaml_ser
print()
print("MsgPack serialization")
import msgpack_ser
print()