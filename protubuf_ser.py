import timeit

setup_pb_ser_code = """import proto.protobuf_ser_pb2 as protobuf_ser_pb2 

obj = protobuf_ser_pb2.D()

obj.words = '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    '''
obj.id = 100
obj.fl = 100.12345
for elem in range(100):
    obj.dct[str(elem)] = 'a'
obj.lst.extend(list(range(100)))
"""

pb_ser_code = """obj.SerializeToString()
"""

setup_pb_deser_code = """import proto.protobuf_ser_pb2 as protobuf_ser_pb2

obj = protobuf_ser_pb2.D()

obj.words = '''
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    '''
obj.id = 100
obj.fl = 100.12345
for elem in range(100):
    obj.dct[str(elem)] = 'a'
obj.lst.extend(list(range(100)))
with open('file_protobuf', 'wb+') as f:
    f.write(obj.SerializeToString())
print('Serialized data len:', len(obj.SerializeToString()))
"""
pb_deser_code = """with open('file_protobuf', 'rb') as f:
    protobuf_ser_pb2.D().ParseFromString(f.read())
"""
print('Serialization time:', timeit.timeit(pb_ser_code, setup=setup_pb_ser_code, number=1000) / 1000)
print('Deserialization time:', timeit.timeit(pb_deser_code, setup=setup_pb_deser_code, number=1000) / 1000)