import timeit

struct_for_serialization = '''d = {
    'words': """
    Lorem ipsum dolor sit amet, consectetur adipiscing
    elit. Mauris adipiscing adipiscing placerat.
    Vestibulum augue augue,
    pellentesque quis sollicitudin id, adipiscing.
    """,
    'list': range(100),
    'dict': dict((str(i),'a') for i in iter(range(100))),
    'int': 100,
    'float': 100.12345,
}'''

setup_native_ser_code = "import pickle\n%s\n" % struct_for_serialization
native_ser_code = "pickle.dumps(d)\n"
setup_native_deser_code = \
    "import pickle\n%s\nwith open('file_native', 'wb+') as f:\n    f.write(pickle.dumps(d))\nprint('Serialized data len:', " \
    "len(pickle.dumps(d)))" % struct_for_serialization
native_deser_code = "with open('file_native', 'rb') as f:\n    s = f.read()\npickle.loads(s)"
print('Serialization time:', timeit.timeit(native_ser_code, setup=setup_native_ser_code, number=1000) / 1000)
print('Deserialization time:', timeit.timeit(native_deser_code, setup=setup_native_deser_code, number=1000) / 1000)
